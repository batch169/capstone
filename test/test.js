const chai = require("chai");
const expect = chai.expect;
const http = require("chai-http");
chai.use(http);

describe("/currency route checking", () => {
  const server = "http://localhost:3001";
  const test = {
    test: "test",
  };
  it("1. Check if POST /currency is running", () => {
    chai
      .request(server)
      .post("/currency")
      .type("json")
      .send(test)
      .end((err, res) => expect(res.status).to.not.equal(undefined));
  });

  it("2. Check if POST /currency returns 400 if name is missing", () => {
    const data = {
      alias: "riyadh",
      ex: {
        peso: 0.47,
        usd: 0.0092,
        won: 10.93,
        yuan: 0.065,
      },
    };

    chai
      .request(server)
      .post("/currency")
      .type("json")
      .send(data)
      .end((err, res) => expect(res.status).to.equal(400));
  });

  it("3. Check if POST /currency returns 400 if name is not a string", () => {
    const data = {
      alias: "riyadh",
      name: 1234,
      ex: {
        peso: 0.47,
        usd: 0.0092,
        won: 10.93,
        yuan: 0.065,
      },
    };

    chai
      .request(server)
      .post("/currency")
      .type("json")
      .send(data)
      .end((err, res) => expect(res.status).to.equal(400));
  });

  it("4. Check if POST /currency returns 400 if name is empty", () => {
    const data = {
      alias: "riyadh",
      name: "",
      ex: {
        peso: 0.47,
        usd: 0.0092,
        won: 10.93,
        yuan: 0.065,
      },
    };

    chai
      .request(server)
      .post("/currency")
      .type("json")
      .send(data)
      .end((err, res) => expect(res.status).to.equal(400));
  });

  it("5. Check if POST /currency returns 400 if ex is missing", () => {
    const data = {
      alias: "riyadh",
      name: "Saudi Arabian Riyadh",
    };

    chai
      .request(server)
      .post("/currency")
      .type("json")
      .send(data)
      .end((err, res) => expect(res.status).to.equal(400));
  });

  it("6. Check if POST /currency returns 400 if ex is not an object", () => {
    let data = {
      alias: "riyadh",
      name: "Saudi Arabian Riyadh",
      ex: "test",
    };

    chai
      .request(server)
      .post("/currency")
      .type("json")
      .send(data)
      .end((err, res) => expect(res.status).to.equal(400));
  });

  it("7. Check if POST /currency returns 400 if ex is empty", () => {
    let data = {
      alias: "riyadh",
      name: "Saudi Arabian Riyadh",
      ex: {},
    };

    chai
      .request(server)
      .post("/currency")
      .type("json")
      .send(data)
      .end((err, res) => expect(res.status).to.equal(400));
  });

  it("8. Check if POST /currency returns 400 if alias is missing", () => {
    let data = {
      name: "Saudi Arabian Riyadh",
      ex: {
        peso: 0.47,
        usd: 0.0092,
        won: 10.93,
        yuan: 0.065,
      },
    };

    chai
      .request(server)
      .post("/currency")
      .type("json")
      .send(data)
      .end((err, res) => expect(res.status).to.equal(400));
  });

  it("9. Check if POST /currency returns 400 if alias is not a string", () => {
    let data = {
      alias: 1234,
      name: "Saudi Arabian Riyadh",
      ex: {
        peso: 0.47,
        usd: 0.0092,
        won: 10.93,
        yuan: 0.065,
      },
    };

    chai
      .request(server)
      .post("/currency")
      .type("json")
      .send(data)
      .end((err, res) => expect(res.status).to.equal(400));
  });

  it("10. Check if POST /currency returns 400 if alias is empty", () => {
    let data = {
      alias: "",
      name: "Saudi Arabian Riyadh",
      ex: {
        peso: 0.47,
        usd: 0.0092,
        won: 10.93,
        yuan: 0.065,
      },
    };

    chai
      .request(server)
      .post("/currency")
      .type("json")
      .send(data)
      .end((err, res) => expect(res.status).to.equal(400));
  });

  it("11. Check if POST /currency returns 400 if all fields are complete, but there is a duplicate alias", () => {
    let data = {
      alias: "peso",
      name: "Saudi Arabian Riyadh",
      ex: {
        peso: 0.47,
        usd: 0.0092,
        won: 10.93,
        yuan: 0.065,
      },
    };

    chai
      .request(server)
      .post("/currency")
      .type("json")
      .send(data)
      .end((err, res) => expect(res.status).to.equal(400));
  });

  it("12. Check if POST /currency returns 200 if all fields are complete, and there are no duplicates", () => {
    let data = {
      alias: "riyadh",
      name: "Saudi Arabian Riyadh",
      ex: {
        peso: 0.47,
        usd: 0.0092,
        won: 10.93,
        yuan: 0.065,
      },
    };

    chai
      .request(server)
      .post("/currency")
      .type("json")
      .send(data)
      .end((err, res) => expect(res.status).to.equal(200));
  });
});
