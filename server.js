const express = require("express");
const app = express();
const PORT = 3001;

app.use(express.json());

require("./routes/routes")(app, {});

app.listen(PORT, () => {
  console.log(`Running on port ${PORT}`);
});
