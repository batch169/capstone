module.exports = (app) => {
  app.post("/currency", (req, res) => {
    const reqBody = req.body;
    const rbAlias = req.body.alias;
    const rbName = req.body.name;
    const rbEx = req.body.ex;

    const data = {
      alias: "peso",
      name: "Philippine Peso",
      ex: {
        peso: 1,
        usd: 0.019,
        won: 23.24,
        yuan: 0.12,
      },
    };

    //check if value exists
    if (
      !reqBody.hasOwnProperty("name") ||
      !reqBody.hasOwnProperty("ex") ||
      !reqBody.hasOwnProperty("alias")
    ) {
      return res.status(400).send({
        error: "Bad Request - missing parameter",
      });
    }

    //check parameter type
    if (
      typeof rbName !== "string" ||
      typeof rbAlias !== "string" ||
      typeof rbEx !== "object"
    ) {
      return res.status(400).send({
        error: "Bad Request - wrong parameter type",
      });
    }

    //check for length
    const rbExlength = Object.keys(rbEx).length;
    if (rbName.length === 0 || rbExlength === 0 || rbAlias.length === 0) {
      return res.status(400).send({
        error: "Bad request - field must not be empty",
      });
    }

    //check if duplicate alias exists
    if (rbAlias === data.alias) {
      return res.status(400).send({
        error: `Value already exists: ${rbAlias} = ${data.alias}`,
      });
    }

    return res.status(200).send("Posted to /currency");
  });
};
